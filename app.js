var http = require('http');
var async = require('async');
var fs = require('fs');
var _ = require('underscore');
var querystring = require('querystring');

var hostname = 'www.zimuzu.tv';
var port = 80;
var default_headers = {
    'User-Agent' : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:45.0) Gecko/20100101 Firefox/45.0",
    'Accept' : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    'Referer' : "http://www.zimuzu.tv/eresourcelist",
    'Accept-Language' : "zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3",
    'Accept-Encoding' : "deflate",
    'Cache-Control': "max-age=0"
};


var urls = {
    checkin: {
        url : '/user/login/getCurUserTopInfo',
        params : {
            "referer" : 'http://www.zimuzu.tv/',
            "X-Requested-With" : 'XMLHttpRequest',
            "Accept" : 'application/json, text/javascript, */*; q=0.01'
        },
        method : 'GET'
    },
    login: {
        url : '/User/Login/ajaxLogin',
        params : {
            "referer" : 'http://www.zimuzu.tv/user/login',
            "Content-Type" : "application/x-www-form-urlencoded",
            "X-Requested-With" : 'XMLHttpRequest',
            "Accept" : 'application/json, text/javascript, */*; q=0.01'
        },
        method : 'POST'
    },
    fav: {
        url : '/user/fav',
        params : {
            "referer" : 'http://www.zimuzu.tv/',
            "Accept" : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        },
        method : 'GET'
    },
    follow : {
        url : '/resource/user_follow',
        params : {
            "referer" : 'http://www.zimuzu.tv/user/fav',
            "X-Requested-With" : 'XMLHttpRequest',
            "Accept" : 'application/json, text/javascript, */*; q=0.01'
        },
        method : 'GET'
    }
}

var cookie_store_path = 'cookie_store.txt';
console.log('zimuzu.tv auto checkin start: ' + new Date().toLocaleString());
var max_try_times = 3;
var now_try_time = 0;

var cookie_string = null;

var do_checkin = function() {
    send_request(urls.checkin, cookie_string, function(err, response){
        if (err) {
            console.log('checkin ERR: ' + JSON.stringify(err));
            return;
        }
        
        console.log(response);
        
        var response_body = {};
        try{
            response_body = JSON.parse(response.body);
        }catch(e) {
            console.log(['checkin response error', response.body].join(':'));
            return;
        }
        
        if (response_body.status == 4001) {
            console.log('need relogin');
            if (now_try_time > max_try_times) {
                console.log('=========================================')
                console.log('tried ' + now_try_time + ' to loing , no luck!');
                console.log('checkin failed!')
                process.exit(1);
            }
            do_login();
            return;
        }
        
        if (response_body.status != 1) {
            console.log(['checkin failed', response.body].join(':'));
            return;
        }
        
        async.series({
            user_fav : function(next) {
                send_request(urls.fav, cookie_string, next);
            },
            user_follow : function(next) {
                send_request(urls.follow, cookie_string, next);
            }
        }, function(err, res) {
            console.log('=========================================')
            console.log('checkin ok!')
        })
    });
}

var do_login = function() {
    now_try_time ++;
    var account_info = require('./account.json');
    var form_data = {
        account : account_info.account,
        password : account_info.password,
        remember : "1",
        url_back : 'http://www.zimuzu.tv/'
    };
    var info = urls.login;
    info.post_data = form_data;
    send_request(info, cookie_string, function(err, response){
        if (err) {
            console.log('ERR: ' + JSON.stringify(err));
            return;
        }
        var response_body = {};
        try{
            response_body = JSON.parse(response.body);
        }catch(e) {
            console.log(['login response error', response.body].join(':'));
            return;
        }
        if (response_body.status != 1) {
            console.log(['login failed', response.body].join(':'));
            return;
        }
        
        var cookie_list = response.headers['set-cookie'];
        var cookies = [];
        _.each(cookie_list, function(item) {
            var cookie = item.split(';')[0];
            var parts = cookie.match(/(.*?)=(.*)$/);
            if (parts && parts[2] != 'deleted') {
                cookies.push(parts[1].trim() + '=' + (parts[2] || '').trim() + ';');
            }
        });
        cookie_string = cookies.join(' ');
        console.log('set cookie: ' + cookie_string);
        fs.writeFileSync(cookie_store_path, cookie_string);
        
        do_checkin();
    });
};



var send_request = function(info, cookie, cb) {
    var headers = JSON.parse(JSON.stringify(default_headers));
    if (info.params) {
        _.each(info.params, function(value, key){
            headers[key] = value;
        });
    };
    headers['cookie'] = cookie;
    var options = {
        hostname : hostname,
        port : port,
        path : info.url,
        method : info.method ? info.method : 'GET',
        headers : headers
    };
    console.log(options);
    
    if (info.post_data) {
        var postData = querystring.stringify(info.post_data);
        headers['Content-Length'] = postData.length
    }
    
    var req = http.request(options, (res) => {
      console.log('TIME:' + new Date());
      console.log(`STATUS: ${res.statusCode}`);
      console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
      res.setEncoding('utf8');
      var result = "";
      var response_headers = res.headers;
      res.on('data', (chunk) => {
        console.log(`BODY: ${chunk}`);
        result += chunk;
      });
      res.on('end', () => {
        return cb(null, {
            headers : response_headers,
            body : result
        });
      })
    });
    req.on('error', (e) => {
      console.log(`problem with request: ${e.message}`);
    });
    if (info.post_data) {
        req.write(postData);
    }
    req.end();
}





try{
    var file_stat = fs.statSync(cookie_store_path);
    if (file_stat && file_stat.isFile()) {
        var file_content = fs.readFileSync(cookie_store_path, {flag: 'r', encoding: 'utf-8'});
        if (file_content.length > 0) {
            cookie_string = file_content.trim();
        }
    }
}catch(e){};


if (!cookie_string) {
    //do login;
    do_login();
} else {
    do_checkin();
}